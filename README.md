# Openflexure Microscope Illumination Controller

*Untested -- work in progress*

A board to control LEDs using an I2C IO expander. Intended for [phase contrast imaging](https://en.wikipedia.org/wiki/Phase-contrast_microscopy) with the [Openflexure Microscope](https://openflexure.org/projects/microscope/).


EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C2
U 1 1 5E30FFC0
P 4100 1300
F 0 "C2" H 4215 1346 50  0000 L CNN
F 1 "10uF" H 4215 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4138 1150 50  0001 C CNN
F 3 "~" H 4100 1300 50  0001 C CNN
	1    4100 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5E31B66B
P 4100 1550
F 0 "#PWR016" H 4100 1300 50  0001 C CNN
F 1 "GND" H 4105 1377 50  0000 C CNN
F 2 "" H 4100 1550 50  0001 C CNN
F 3 "" H 4100 1550 50  0001 C CNN
	1    4100 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 1450 4100 1550
$Comp
L Device:C C1
U 1 1 5E335546
P 3700 1300
F 0 "C1" H 3815 1346 50  0000 L CNN
F 1 "0.1uF" H 3815 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3738 1150 50  0001 C CNN
F 3 "~" H 3700 1300 50  0001 C CNN
	1    3700 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5E336D12
P 3700 1550
F 0 "#PWR013" H 3700 1300 50  0001 C CNN
F 1 "GND" H 3705 1377 50  0000 C CNN
F 2 "" H 3700 1550 50  0001 C CNN
F 3 "" H 3700 1550 50  0001 C CNN
	1    3700 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 1550 3700 1450
$Comp
L NSI45020T1G:NSI45020T1G D1
U 1 1 5E53F99A
P 5700 3250
F 0 "D1" V 5746 3170 50  0000 R CNN
F 1 "NSI45020T1G" V 5655 3170 50  0000 R CNN
F 2 "Custom:SOD3716X135N" H 5700 3250 50  0001 L BNN
F 3 "6" H 5700 3250 50  0001 L BNN
F 4 "ON Semiconductor" H 5700 3250 50  0001 L BNN "Field4"
F 5 "IPC-7351B" H 5700 3250 50  0001 L BNN "Field5"
F 6 "5492" H 5700 3250 50  0001 L BNN "Field6"
	1    5700 3250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x04_Male J2
U 1 1 5E5645F1
P 1350 2000
F 0 "J2" H 1458 2281 50  0000 C CNN
F 1 "Conn_01x04_Male" H 1458 2190 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 1350 2000 50  0001 C CNN
F 3 "~" H 1350 2000 50  0001 C CNN
F 4 "22053041" H 1350 2000 50  0001 C CNN "MPN"
	1    1350 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR03
U 1 1 5E566250
P 1850 1700
F 0 "#PWR03" H 1850 1550 50  0001 C CNN
F 1 "+3.3V" H 1865 1873 50  0000 C CNN
F 2 "" H 1850 1700 50  0001 C CNN
F 3 "" H 1850 1700 50  0001 C CNN
	1    1850 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 1900 1850 1900
Wire Wire Line
	1850 1900 1850 1700
$Comp
L power:GND #PWR07
U 1 1 5E56715C
P 2050 2000
F 0 "#PWR07" H 2050 1750 50  0001 C CNN
F 1 "GND" H 2055 1827 50  0000 C CNN
F 2 "" H 2050 2000 50  0001 C CNN
F 3 "" H 2050 2000 50  0001 C CNN
	1    2050 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 2100 1700 2100
Wire Wire Line
	1550 2200 1700 2200
$Comp
L power:+3.3V #PWR014
U 1 1 5E571BEC
P 3450 3250
F 0 "#PWR014" H 3450 3100 50  0001 C CNN
F 1 "+3.3V" H 3465 3423 50  0000 C CNN
F 2 "" H 3450 3250 50  0001 C CNN
F 3 "" H 3450 3250 50  0001 C CNN
	1    3450 3250
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR012
U 1 1 5E573621
P 3700 1050
F 0 "#PWR012" H 3700 900 50  0001 C CNN
F 1 "+3.3V" H 3715 1223 50  0000 C CNN
F 2 "" H 3700 1050 50  0001 C CNN
F 3 "" H 3700 1050 50  0001 C CNN
	1    3700 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 1050 3700 1100
$Comp
L Connector:Conn_01x04_Male J3
U 1 1 5E575471
P 1350 2950
F 0 "J3" H 1458 3231 50  0000 C CNN
F 1 "Conn_01x04_Male" H 1458 3140 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 1350 2950 50  0001 C CNN
F 3 "~" H 1350 2950 50  0001 C CNN
F 4 "22053041" H 1350 2950 50  0001 C CNN "MPN"
	1    1350 2950
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR04
U 1 1 5E57547B
P 1850 2650
F 0 "#PWR04" H 1850 2500 50  0001 C CNN
F 1 "+3.3V" H 1865 2823 50  0000 C CNN
F 2 "" H 1850 2650 50  0001 C CNN
F 3 "" H 1850 2650 50  0001 C CNN
	1    1850 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 2850 1850 2850
Wire Wire Line
	1850 2850 1850 2650
$Comp
L power:GND #PWR08
U 1 1 5E575489
P 2050 2950
F 0 "#PWR08" H 2050 2700 50  0001 C CNN
F 1 "GND" H 2055 2777 50  0000 C CNN
F 2 "" H 2050 2950 50  0001 C CNN
F 3 "" H 2050 2950 50  0001 C CNN
	1    2050 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 3050 1700 3050
Wire Wire Line
	1550 3150 1700 3150
Text GLabel 1700 2100 2    50   Input ~ 0
SCL
Wire Wire Line
	1550 2000 2050 2000
Text GLabel 1700 3050 2    50   Input ~ 0
SCL
Text GLabel 2500 3900 0    50   Input ~ 0
SCL
Wire Wire Line
	1550 2950 2050 2950
Text GLabel 1700 2200 2    50   Input ~ 0
SDA
Text GLabel 1700 3150 2    50   Input ~ 0
SDA
Text GLabel 2500 4000 0    50   Input ~ 0
SDA
$Comp
L power:GND #PWR015
U 1 1 5E587316
P 3150 5400
F 0 "#PWR015" H 3150 5150 50  0001 C CNN
F 1 "GND" H 3155 5227 50  0000 C CNN
F 2 "" H 3150 5400 50  0001 C CNN
F 3 "" H 3150 5400 50  0001 C CNN
	1    3150 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 5250 3150 5400
$Comp
L Device:R_Small R7
U 1 1 5E588525
P 2450 3450
F 0 "R7" H 2509 3496 50  0000 L CNN
F 1 "10k" H 2509 3405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2450 3450 50  0001 C CNN
F 3 "~" H 2450 3450 50  0001 C CNN
	1    2450 3450
	1    0    0    -1  
$EndComp
Text GLabel 2500 4200 0    50   Input ~ 0
A0
Text GLabel 2500 4300 0    50   Input ~ 0
A1
Text GLabel 2500 4400 0    50   Input ~ 0
A2
Text GLabel 500  5900 0    50   Input ~ 0
A0
$Comp
L Device:R_Small R2
U 1 1 5E591E4E
P 650 6050
F 0 "R2" H 591 6004 50  0000 R CNN
F 1 "10k" H 591 6095 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 650 6050 50  0001 C CNN
F 3 "~" H 650 6050 50  0001 C CNN
	1    650  6050
	-1   0    0    1   
$EndComp
Wire Wire Line
	500  5900 650  5900
Wire Wire Line
	650  5900 650  5950
$Comp
L power:GND #PWR02
U 1 1 5E59D695
P 650 6250
F 0 "#PWR02" H 650 6000 50  0001 C CNN
F 1 "GND" H 655 6077 50  0000 C CNN
F 2 "" H 650 6250 50  0001 C CNN
F 3 "" H 650 6250 50  0001 C CNN
	1    650  6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	650  6150 650  6250
$Comp
L Device:R_Small R1
U 1 1 5E59DF1E
P 650 5750
F 0 "R1" H 591 5704 50  0000 R CNN
F 1 "DNP" H 591 5795 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 650 5750 50  0001 C CNN
F 3 "~" H 650 5750 50  0001 C CNN
	1    650  5750
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR01
U 1 1 5E59E5D3
P 650 5550
F 0 "#PWR01" H 650 5400 50  0001 C CNN
F 1 "+3.3V" H 665 5723 50  0000 C CNN
F 2 "" H 650 5550 50  0001 C CNN
F 3 "" H 650 5550 50  0001 C CNN
	1    650  5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	650  5550 650  5650
Wire Wire Line
	650  5850 650  5900
Connection ~ 650  5900
Text GLabel 1000 6450 0    50   Input ~ 0
A1
$Comp
L Device:R_Small R4
U 1 1 5E5A26BD
P 1150 6600
F 0 "R4" H 1091 6554 50  0000 R CNN
F 1 "10k" H 1091 6645 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1150 6600 50  0001 C CNN
F 3 "~" H 1150 6600 50  0001 C CNN
	1    1150 6600
	-1   0    0    1   
$EndComp
Wire Wire Line
	1000 6450 1150 6450
Wire Wire Line
	1150 6450 1150 6500
$Comp
L power:GND #PWR06
U 1 1 5E5A26C9
P 1150 6800
F 0 "#PWR06" H 1150 6550 50  0001 C CNN
F 1 "GND" H 1155 6627 50  0000 C CNN
F 2 "" H 1150 6800 50  0001 C CNN
F 3 "" H 1150 6800 50  0001 C CNN
	1    1150 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 6700 1150 6800
$Comp
L Device:R_Small R3
U 1 1 5E5A26D4
P 1150 6300
F 0 "R3" H 1091 6254 50  0000 R CNN
F 1 "DNP" H 1091 6345 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1150 6300 50  0001 C CNN
F 3 "~" H 1150 6300 50  0001 C CNN
	1    1150 6300
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR05
U 1 1 5E5A26DE
P 1150 6100
F 0 "#PWR05" H 1150 5950 50  0001 C CNN
F 1 "+3.3V" H 1165 6273 50  0000 C CNN
F 2 "" H 1150 6100 50  0001 C CNN
F 3 "" H 1150 6100 50  0001 C CNN
	1    1150 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 6100 1150 6200
Wire Wire Line
	1150 6400 1150 6450
Connection ~ 1150 6450
Text GLabel 1400 7200 0    50   Input ~ 0
A2
$Comp
L Device:R_Small R6
U 1 1 5E5A556E
P 1550 7350
F 0 "R6" H 1491 7304 50  0000 R CNN
F 1 "10k" H 1491 7395 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1550 7350 50  0001 C CNN
F 3 "~" H 1550 7350 50  0001 C CNN
	1    1550 7350
	-1   0    0    1   
$EndComp
Wire Wire Line
	1400 7200 1550 7200
Wire Wire Line
	1550 7200 1550 7250
$Comp
L power:GND #PWR010
U 1 1 5E5A557A
P 1550 7550
F 0 "#PWR010" H 1550 7300 50  0001 C CNN
F 1 "GND" H 1555 7377 50  0000 C CNN
F 2 "" H 1550 7550 50  0001 C CNN
F 3 "" H 1550 7550 50  0001 C CNN
	1    1550 7550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 7450 1550 7550
$Comp
L Device:R_Small R5
U 1 1 5E5A5585
P 1550 7050
F 0 "R5" H 1491 7004 50  0000 R CNN
F 1 "DNP" H 1491 7095 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1550 7050 50  0001 C CNN
F 3 "~" H 1550 7050 50  0001 C CNN
	1    1550 7050
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR09
U 1 1 5E5A558F
P 1550 6850
F 0 "#PWR09" H 1550 6700 50  0001 C CNN
F 1 "+3.3V" H 1565 7023 50  0000 C CNN
F 2 "" H 1550 6850 50  0001 C CNN
F 3 "" H 1550 6850 50  0001 C CNN
	1    1550 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 6850 1550 6950
Wire Wire Line
	1550 7150 1550 7200
Connection ~ 1550 7200
Wire Wire Line
	2450 3650 2450 3550
$Comp
L power:+3.3V #PWR011
U 1 1 5E5C03BA
P 2450 3300
F 0 "#PWR011" H 2450 3150 50  0001 C CNN
F 1 "+3.3V" H 2465 3473 50  0000 C CNN
F 2 "" H 2450 3300 50  0001 C CNN
F 3 "" H 2450 3300 50  0001 C CNN
	1    2450 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 3300 2450 3350
Text GLabel 2350 3650 0    50   Input ~ 0
RESET
Wire Wire Line
	2350 3650 2450 3650
Connection ~ 2450 3650
Wire Wire Line
	2450 3650 2550 3650
Text GLabel 1750 1000 2    50   Input ~ 0
RESET
Wire Wire Line
	1550 1000 1750 1000
Wire Wire Line
	4100 1150 4100 1100
Wire Wire Line
	4100 1100 3700 1100
Connection ~ 3700 1100
Wire Wire Line
	3700 1100 3700 1150
$Comp
L NSI45020T1G:NSI45020T1G D8
U 1 1 5E5545BC
P 10650 3250
F 0 "D8" V 10696 3170 50  0000 R CNN
F 1 "NSI45020T1G" V 10605 3170 50  0000 R CNN
F 2 "Custom:SOD3716X135N" H 10650 3250 50  0001 L BNN
F 3 "6" H 10650 3250 50  0001 L BNN
F 4 "ON Semiconductor" H 10650 3250 50  0001 L BNN "Field4"
F 5 "IPC-7351B" H 10650 3250 50  0001 L BNN "Field5"
F 6 "5492" H 10650 3250 50  0001 L BNN "Field6"
	1    10650 3250
	0    -1   -1   0   
$EndComp
$Comp
L NSI45020T1G:NSI45020T1G D7
U 1 1 5E5545AF
P 9950 3250
F 0 "D7" V 9996 3170 50  0000 R CNN
F 1 "NSI45020T1G" V 9905 3170 50  0000 R CNN
F 2 "Custom:SOD3716X135N" H 9950 3250 50  0001 L BNN
F 3 "6" H 9950 3250 50  0001 L BNN
F 4 "ON Semiconductor" H 9950 3250 50  0001 L BNN "Field4"
F 5 "IPC-7351B" H 9950 3250 50  0001 L BNN "Field5"
F 6 "5492" H 9950 3250 50  0001 L BNN "Field6"
	1    9950 3250
	0    -1   -1   0   
$EndComp
$Comp
L NSI45020T1G:NSI45020T1G D6
U 1 1 5E5545A2
P 9250 3250
F 0 "D6" V 9296 3170 50  0000 R CNN
F 1 "NSI45020T1G" V 9205 3170 50  0000 R CNN
F 2 "Custom:SOD3716X135N" H 9250 3250 50  0001 L BNN
F 3 "6" H 9250 3250 50  0001 L BNN
F 4 "ON Semiconductor" H 9250 3250 50  0001 L BNN "Field4"
F 5 "IPC-7351B" H 9250 3250 50  0001 L BNN "Field5"
F 6 "5492" H 9250 3250 50  0001 L BNN "Field6"
	1    9250 3250
	0    -1   -1   0   
$EndComp
$Comp
L NSI45020T1G:NSI45020T1G D5
U 1 1 5E554595
P 8550 3250
F 0 "D5" V 8596 3170 50  0000 R CNN
F 1 "NSI45020T1G" V 8505 3170 50  0000 R CNN
F 2 "Custom:SOD3716X135N" H 8550 3250 50  0001 L BNN
F 3 "6" H 8550 3250 50  0001 L BNN
F 4 "ON Semiconductor" H 8550 3250 50  0001 L BNN "Field4"
F 5 "IPC-7351B" H 8550 3250 50  0001 L BNN "Field5"
F 6 "5492" H 8550 3250 50  0001 L BNN "Field6"
	1    8550 3250
	0    -1   -1   0   
$EndComp
$Comp
L NSI45020T1G:NSI45020T1G D4
U 1 1 5E551DF8
P 7850 3250
F 0 "D4" V 7896 3170 50  0000 R CNN
F 1 "NSI45020T1G" V 7805 3170 50  0000 R CNN
F 2 "Custom:SOD3716X135N" H 7850 3250 50  0001 L BNN
F 3 "6" H 7850 3250 50  0001 L BNN
F 4 "ON Semiconductor" H 7850 3250 50  0001 L BNN "Field4"
F 5 "IPC-7351B" H 7850 3250 50  0001 L BNN "Field5"
F 6 "5492" H 7850 3250 50  0001 L BNN "Field6"
	1    7850 3250
	0    -1   -1   0   
$EndComp
$Comp
L NSI45020T1G:NSI45020T1G D3
U 1 1 5E551DEB
P 7100 3250
F 0 "D3" V 7146 3170 50  0000 R CNN
F 1 "NSI45020T1G" V 7055 3170 50  0000 R CNN
F 2 "Custom:SOD3716X135N" H 7100 3250 50  0001 L BNN
F 3 "6" H 7100 3250 50  0001 L BNN
F 4 "ON Semiconductor" H 7100 3250 50  0001 L BNN "Field4"
F 5 "IPC-7351B" H 7100 3250 50  0001 L BNN "Field5"
F 6 "5492" H 7100 3250 50  0001 L BNN "Field6"
	1    7100 3250
	0    -1   -1   0   
$EndComp
$Comp
L NSI45020T1G:NSI45020T1G D2
U 1 1 5E54E4C2
P 6400 3250
F 0 "D2" V 6446 3170 50  0000 R CNN
F 1 "NSI45020T1G" V 6355 3170 50  0000 R CNN
F 2 "Custom:SOD3716X135N" H 6400 3250 50  0001 L BNN
F 3 "6" H 6400 3250 50  0001 L BNN
F 4 "ON Semiconductor" H 6400 3250 50  0001 L BNN "Field4"
F 5 "IPC-7351B" H 6400 3250 50  0001 L BNN "Field5"
F 6 "5492" H 6400 3250 50  0001 L BNN "Field6"
	1    6400 3250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5700 3450 5700 4050
Wire Wire Line
	5700 4050 3750 4050
Wire Wire Line
	7850 3450 7850 4350
Wire Wire Line
	7850 4350 7800 4350
Wire Wire Line
	8550 3450 8550 4450
Wire Wire Line
	9250 3450 9250 4550
Wire Wire Line
	9950 4650 3750 4650
Text GLabel 2450 4800 0    50   Input ~ 0
INTERRUPT_OUTPUT
Text GLabel 1750 1100 2    50   Input ~ 0
INTERRUPT_OUTPUT
Wire Wire Line
	1750 1100 1550 1100
$Comp
L Device:Jumper JP1
U 1 1 5E781F70
P 4900 4900
F 0 "JP1" H 4900 5100 50  0000 C CNN
F 1 "Jumper" H 4900 5200 50  0000 C CNN
F 2 "4ms/4ms-kicad-lib/4ms-footprints.pretty:JUMPER_SMD_1x2" H 4900 4900 50  0001 C CNN
F 3 "~" H 4900 4900 50  0001 C CNN
	1    4900 4900
	-1   0    0    1   
$EndComp
Wire Wire Line
	4600 4900 4400 4900
Wire Wire Line
	5400 4900 5400 4750
Wire Wire Line
	4400 4750 4400 4900
Wire Wire Line
	5400 4900 5200 4900
$Comp
L MCP23008-E_ML:MCP23008-E_ML U1
U 1 1 5E62B45B
P 3150 4300
F 0 "U1" H 3150 5217 50  0000 C CNN
F 1 "MCP23008-E_ML" H 3050 5100 50  0000 C CNN
F 2 "Custom:QFN21P50_400X400X100L40X24T270N" H 3150 4300 50  0001 L BNN
F 3 "Microchip" H 3150 4300 50  0001 L BNN
	1    3150 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 3250 3450 3350
Wire Wire Line
	7100 4250 3750 4250
Wire Wire Line
	3750 4450 8500 4450
Wire Wire Line
	3750 4550 9200 4550
Wire Wire Line
	3750 4750 4400 4750
Wire Wire Line
	5400 4750 10650 4750
Wire Wire Line
	5400 4750 4400 4750
Connection ~ 5400 4750
Connection ~ 4400 4750
Wire Wire Line
	2500 3900 2550 3900
Wire Wire Line
	2500 4000 2550 4000
Wire Wire Line
	2450 4800 2550 4800
Wire Wire Line
	2500 4200 2550 4200
Wire Wire Line
	2500 4300 2550 4300
Wire Wire Line
	2500 4400 2550 4400
Wire Wire Line
	5700 2850 5700 3050
Wire Wire Line
	6400 2850 6400 3050
Wire Wire Line
	10650 2850 10650 3050
Wire Wire Line
	9950 2850 9950 3050
Wire Wire Line
	9250 2850 9250 3050
Wire Wire Line
	8550 2850 8550 3050
Wire Wire Line
	7850 2850 7850 3050
Wire Wire Line
	7100 2850 7100 3050
Wire Wire Line
	10350 6000 10350 5900
Wire Wire Line
	5700 2500 6400 2500
Wire Wire Line
	5700 2550 5700 2500
Wire Wire Line
	6400 2500 7100 2500
Connection ~ 6400 2500
Wire Wire Line
	6400 2550 6400 2500
Wire Wire Line
	7100 2500 7850 2500
Connection ~ 7100 2500
Wire Wire Line
	7100 2550 7100 2500
Wire Wire Line
	7850 2500 8550 2500
Connection ~ 7850 2500
Wire Wire Line
	7850 2550 7850 2500
Wire Wire Line
	8550 2500 9250 2500
Connection ~ 8550 2500
Wire Wire Line
	8550 2550 8550 2500
Wire Wire Line
	9250 2500 9950 2500
Connection ~ 9250 2500
Wire Wire Line
	9250 2550 9250 2500
Wire Wire Line
	9950 2500 10650 2500
Connection ~ 9950 2500
Wire Wire Line
	9950 2550 9950 2500
Wire Wire Line
	10650 2500 10650 2550
Wire Wire Line
	10350 6000 9950 6000
$Comp
L power:+3.3V #PWR017
U 1 1 5E69E62E
P 10350 5900
F 0 "#PWR017" H 10350 5750 50  0001 C CNN
F 1 "+3.3V" H 10365 6073 50  0000 C CNN
F 2 "" H 10350 5900 50  0001 C CNN
F 3 "" H 10350 5900 50  0001 C CNN
	1    10350 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:LED LED1
U 1 1 5E309E23
P 5700 2700
F 0 "LED1" V 5739 2583 50  0000 R CNN
F 1 "LED" V 5648 2583 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 5700 2700 50  0001 C CNN
F 3 "~" H 5700 2700 50  0001 C CNN
	1    5700 2700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED LED2
U 1 1 5E30A5EC
P 6400 2700
F 0 "LED2" V 6439 2583 50  0000 R CNN
F 1 "LED" V 6348 2583 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 6400 2700 50  0001 C CNN
F 3 "~" H 6400 2700 50  0001 C CNN
	1    6400 2700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED LED8
U 1 1 5E30E0F9
P 10650 2700
F 0 "LED8" V 10689 2583 50  0000 R CNN
F 1 "LED" V 10598 2583 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 10650 2700 50  0001 C CNN
F 3 "~" H 10650 2700 50  0001 C CNN
	1    10650 2700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED LED7
U 1 1 5E30E0EF
P 9950 2700
F 0 "LED7" V 9989 2583 50  0000 R CNN
F 1 "LED" V 9898 2583 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 9950 2700 50  0001 C CNN
F 3 "~" H 9950 2700 50  0001 C CNN
	1    9950 2700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED LED6
U 1 1 5E30E0E5
P 9250 2700
F 0 "LED6" V 9289 2583 50  0000 R CNN
F 1 "LED" V 9198 2583 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 9250 2700 50  0001 C CNN
F 3 "~" H 9250 2700 50  0001 C CNN
	1    9250 2700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED LED5
U 1 1 5E30E0DB
P 8550 2700
F 0 "LED5" V 8589 2583 50  0000 R CNN
F 1 "LED" V 8498 2583 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 8550 2700 50  0001 C CNN
F 3 "~" H 8550 2700 50  0001 C CNN
	1    8550 2700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED LED4
U 1 1 5E30CB19
P 7850 2700
F 0 "LED4" V 7889 2583 50  0000 R CNN
F 1 "LED" V 7798 2583 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 7850 2700 50  0001 C CNN
F 3 "~" H 7850 2700 50  0001 C CNN
	1    7850 2700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED LED3
U 1 1 5E30CB0F
P 7100 2700
F 0 "LED3" V 7139 2583 50  0000 R CNN
F 1 "LED" V 7048 2583 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 7100 2700 50  0001 C CNN
F 3 "~" H 7100 2700 50  0001 C CNN
	1    7100 2700
	0    -1   -1   0   
$EndComp
$Comp
L NSI45020T1G:NSI45020T1G D9
U 1 1 5E6D54C5
P 6400 5250
F 0 "D9" V 6446 5170 50  0000 R CNN
F 1 "NSI45020T1G" V 6355 5170 50  0000 R CNN
F 2 "Custom:SOD3716X135N" H 6400 5250 50  0001 L BNN
F 3 "6" H 6400 5250 50  0001 L BNN
F 4 "ON Semiconductor" H 6400 5250 50  0001 L BNN "Field4"
F 5 "IPC-7351B" H 6400 5250 50  0001 L BNN "Field5"
F 6 "5492" H 6400 5250 50  0001 L BNN "Field6"
	1    6400 5250
	0    1    1    0   
$EndComp
$Comp
L NSI45020T1G:NSI45020T1G D10
U 1 1 5E6D54D2
P 7100 5250
F 0 "D10" V 7146 5170 50  0000 R CNN
F 1 "NSI45020T1G" V 7055 5170 50  0000 R CNN
F 2 "Custom:SOD3716X135N" H 7100 5250 50  0001 L BNN
F 3 "6" H 7100 5250 50  0001 L BNN
F 4 "ON Semiconductor" H 7100 5250 50  0001 L BNN "Field4"
F 5 "IPC-7351B" H 7100 5250 50  0001 L BNN "Field5"
F 6 "5492" H 7100 5250 50  0001 L BNN "Field6"
	1    7100 5250
	0    1    1    0   
$EndComp
$Comp
L NSI45020T1G:NSI45020T1G D11
U 1 1 5E6D54DF
P 7800 5250
F 0 "D11" V 7846 5170 50  0000 R CNN
F 1 "NSI45020T1G" V 7755 5170 50  0000 R CNN
F 2 "Custom:SOD3716X135N" H 7800 5250 50  0001 L BNN
F 3 "6" H 7800 5250 50  0001 L BNN
F 4 "ON Semiconductor" H 7800 5250 50  0001 L BNN "Field4"
F 5 "IPC-7351B" H 7800 5250 50  0001 L BNN "Field5"
F 6 "5492" H 7800 5250 50  0001 L BNN "Field6"
	1    7800 5250
	0    1    1    0   
$EndComp
$Comp
L NSI45020T1G:NSI45020T1G D12
U 1 1 5E6D54EC
P 8500 5250
F 0 "D12" V 8546 5170 50  0000 R CNN
F 1 "NSI45020T1G" V 8455 5170 50  0000 R CNN
F 2 "Custom:SOD3716X135N" H 8500 5250 50  0001 L BNN
F 3 "6" H 8500 5250 50  0001 L BNN
F 4 "ON Semiconductor" H 8500 5250 50  0001 L BNN "Field4"
F 5 "IPC-7351B" H 8500 5250 50  0001 L BNN "Field5"
F 6 "5492" H 8500 5250 50  0001 L BNN "Field6"
	1    8500 5250
	0    1    1    0   
$EndComp
$Comp
L NSI45020T1G:NSI45020T1G D13
U 1 1 5E6D54F9
P 9200 5250
F 0 "D13" V 9246 5170 50  0000 R CNN
F 1 "NSI45020T1G" V 9155 5170 50  0000 R CNN
F 2 "Custom:SOD3716X135N" H 9200 5250 50  0001 L BNN
F 3 "6" H 9200 5250 50  0001 L BNN
F 4 "ON Semiconductor" H 9200 5250 50  0001 L BNN "Field4"
F 5 "IPC-7351B" H 9200 5250 50  0001 L BNN "Field5"
F 6 "5492" H 9200 5250 50  0001 L BNN "Field6"
	1    9200 5250
	0    1    1    0   
$EndComp
$Comp
L NSI45020T1G:NSI45020T1G D14
U 1 1 5E6D5506
P 9950 5250
F 0 "D14" V 9996 5170 50  0000 R CNN
F 1 "NSI45020T1G" V 9905 5170 50  0000 R CNN
F 2 "Custom:SOD3716X135N" H 9950 5250 50  0001 L BNN
F 3 "6" H 9950 5250 50  0001 L BNN
F 4 "ON Semiconductor" H 9950 5250 50  0001 L BNN "Field4"
F 5 "IPC-7351B" H 9950 5250 50  0001 L BNN "Field5"
F 6 "5492" H 9950 5250 50  0001 L BNN "Field6"
	1    9950 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	9950 5050 9950 4650
Wire Wire Line
	8500 5050 8500 4450
Wire Wire Line
	7800 5050 7800 4350
Wire Wire Line
	7100 5050 7100 4250
Wire Wire Line
	6400 5650 6400 5450
Wire Wire Line
	7100 5650 7100 5450
Wire Wire Line
	7800 5650 7800 5450
Wire Wire Line
	8500 5650 8500 5450
Wire Wire Line
	9200 5650 9200 5450
Wire Wire Line
	9950 5650 9950 5450
Wire Wire Line
	9950 6000 9200 6000
Wire Wire Line
	9950 5950 9950 6000
Wire Wire Line
	9200 6000 8500 6000
Connection ~ 9200 6000
Wire Wire Line
	9200 5950 9200 6000
Wire Wire Line
	8500 6000 7800 6000
Connection ~ 8500 6000
Wire Wire Line
	8500 5950 8500 6000
Wire Wire Line
	7800 6000 7100 6000
Connection ~ 7800 6000
Wire Wire Line
	7800 5950 7800 6000
Wire Wire Line
	7100 6000 6400 6000
Connection ~ 7100 6000
Wire Wire Line
	7100 5950 7100 6000
Wire Wire Line
	6400 6000 6400 5950
$Comp
L Device:LED LED9
U 1 1 5E6D5563
P 6400 5800
F 0 "LED9" V 6439 5683 50  0000 R CNN
F 1 "LED" V 6348 5683 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 6400 5800 50  0001 C CNN
F 3 "~" H 6400 5800 50  0001 C CNN
	1    6400 5800
	0    1    1    0   
$EndComp
$Comp
L Device:LED LED10
U 1 1 5E6D556D
P 7100 5800
F 0 "LED10" V 7139 5683 50  0000 R CNN
F 1 "LED" V 7048 5683 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 7100 5800 50  0001 C CNN
F 3 "~" H 7100 5800 50  0001 C CNN
	1    7100 5800
	0    1    1    0   
$EndComp
$Comp
L Device:LED LED11
U 1 1 5E6D5577
P 7800 5800
F 0 "LED11" V 7839 5683 50  0000 R CNN
F 1 "LED" V 7748 5683 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 7800 5800 50  0001 C CNN
F 3 "~" H 7800 5800 50  0001 C CNN
	1    7800 5800
	0    1    1    0   
$EndComp
$Comp
L Device:LED LED12
U 1 1 5E6D5581
P 8500 5800
F 0 "LED12" V 8539 5683 50  0000 R CNN
F 1 "LED" V 8448 5683 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 8500 5800 50  0001 C CNN
F 3 "~" H 8500 5800 50  0001 C CNN
	1    8500 5800
	0    1    1    0   
$EndComp
$Comp
L Device:LED LED13
U 1 1 5E6D558B
P 9200 5800
F 0 "LED13" V 9239 5683 50  0000 R CNN
F 1 "LED" V 9148 5683 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 9200 5800 50  0001 C CNN
F 3 "~" H 9200 5800 50  0001 C CNN
	1    9200 5800
	0    1    1    0   
$EndComp
$Comp
L Device:LED LED14
U 1 1 5E6D5595
P 9950 5800
F 0 "LED14" V 9989 5683 50  0000 R CNN
F 1 "LED" V 9898 5683 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 9950 5800 50  0001 C CNN
F 3 "~" H 9950 5800 50  0001 C CNN
	1    9950 5800
	0    1    1    0   
$EndComp
Connection ~ 7100 4250
Connection ~ 7800 4350
Wire Wire Line
	7800 4350 3750 4350
Connection ~ 8500 4450
Wire Wire Line
	8500 4450 8550 4450
Connection ~ 9950 4650
Wire Wire Line
	6400 5050 6400 4150
Connection ~ 6400 4150
Wire Wire Line
	6400 4150 3750 4150
Wire Wire Line
	6400 3450 6400 4150
Wire Wire Line
	7100 3450 7100 4250
Wire Wire Line
	9950 3450 9950 4650
Wire Wire Line
	10650 3450 10650 4750
Connection ~ 9200 4550
Wire Wire Line
	9200 4550 9250 4550
Wire Wire Line
	9200 4550 9200 5050
$Comp
L Connector:Conn_01x01_Female H1
U 1 1 5E88D092
P 5400 7000
F 0 "H1" H 5428 7026 50  0000 L CNN
F 1 "Conn_01x01_Female" H 5428 6935 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380" H 5400 7000 50  0001 C CNN
F 3 "~" H 5400 7000 50  0001 C CNN
	1    5400 7000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female H2
U 1 1 5E88D8C1
P 5350 7400
F 0 "H2" H 5378 7426 50  0000 L CNN
F 1 "Conn_01x01_Female" H 5378 7335 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380" H 5350 7400 50  0001 C CNN
F 3 "~" H 5350 7400 50  0001 C CNN
	1    5350 7400
	1    0    0    -1  
$EndComp
Connection ~ 9950 6000
Wire Wire Line
	5300 2500 5700 2500
$Comp
L power:+3.3V #PWR0101
U 1 1 5E8F18B6
P 5300 2300
F 0 "#PWR0101" H 5300 2150 50  0001 C CNN
F 1 "+3.3V" H 5315 2473 50  0000 C CNN
F 2 "" H 5300 2300 50  0001 C CNN
F 3 "" H 5300 2300 50  0001 C CNN
	1    5300 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2500 5300 2300
$Comp
L Connector:Conn_01x01_Female LOGO1
U 1 1 5E705A4C
P 5750 6400
F 0 "LOGO1" H 5778 6426 50  0000 L CNN
F 1 "Conn_01x01_Female" H 5778 6335 50  0000 L CNN
F 2 "Custom:logo" H 5750 6400 50  0001 C CNN
F 3 "~" H 5750 6400 50  0001 C CNN
	1    5750 6400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5E70677A
P 5500 6500
F 0 "#PWR0102" H 5500 6250 50  0001 C CNN
F 1 "GND" H 5505 6327 50  0000 C CNN
F 2 "" H 5500 6500 50  0001 C CNN
F 3 "" H 5500 6500 50  0001 C CNN
	1    5500 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 6400 5500 6400
Wire Wire Line
	5500 6400 5500 6500
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5E5C50EF
P 1350 1100
F 0 "J1" H 1458 1281 50  0000 C CNN
F 1 "Conn_01x02_Male" H 1458 1190 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1350 1100 50  0001 C CNN
F 3 "~" H 1350 1100 50  0001 C CNN
	1    1350 1100
	1    0    0    1   
$EndComp
$EndSCHEMATC
